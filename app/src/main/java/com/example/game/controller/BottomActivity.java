package com.example.game.controller;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.game.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView.OnItemSelectedListener;


public class BottomActivity extends AppCompatActivity {
    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilpage);
        mBottomNavigationView=findViewById(R.id.bottom_navigation);
        mBottomNavigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                Log.d("MesLogs", "je clique");
                switch (item.getItemId()){
                    case R.id.action_profile:
                        Log.d("MesLogs", "Profile");
                        Intent intent = new Intent(BottomActivity.this, ProfilepageActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.action_groupe:
                        Log.d("MesLogs", "History");
                        Intent history = new Intent(BottomActivity.this, HistoryActivity.class);
                        startActivity(history);
                        break;

                    case R.id.action_game:
                        Log.d("MesLogs", "Game");
                        Intent game = new Intent(BottomActivity.this, GameActivity.class);
                        startActivity(game);
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_content, selectedFragment);
                transaction.commit();
                return true;
            }
        });
    }
}

