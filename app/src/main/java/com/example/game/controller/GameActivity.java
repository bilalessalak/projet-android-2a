package com.example.game.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.game.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class GameActivity extends AppCompatActivity {
    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        mBottomNavigationView=findViewById(R.id.bottom_navigation);
        mBottomNavigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Log.d("MesLogs", "je clique");
                switch (item.getItemId()){
                    case R.id.action_profile:
                        Log.d("MesLogs", "Profile");

                        Intent intent = new Intent(GameActivity.this, ProfilepageActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.action_groupe:
                        Log.d("MesLogs", "History");
                        Intent history = new Intent(GameActivity.this, HistoryActivity.class);
                        startActivity(history);
                        break;

                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_content, null);
                transaction.commit();
                return true;
            }
        });
    }
}

