package com.example.game.controller;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.game.R;
import com.example.game.data.GroupeDao;
import com.example.game.data.UserDao;
import com.example.game.data.UserDataBase;
import com.example.game.model.Groupe;
import com.example.game.model.User;
import com.example.game.views.HistoryAdapter;
import com.example.game.views.HistoryViewholder;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;
import java.util.Random;
import android.content.Context;



public class HistoryActivity extends AppCompatActivity implements DialogActivity.ExampleDialogListener {

    private BottomNavigationView mBottomNavigationView;
    private Button btn_creerGroupe;
    private String nom_groupe;

    private TextView textViewUsername;

    private HistoryViewholder historyViewHolder;
    private View v;

    GroupeDao groupeDao;


    @RequiresApi(api = Build.VERSION_CODES.O)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        btn_creerGroupe = findViewById(R.id.creerGroupe);

        textViewUsername = (TextView) findViewById(R.id.textview_username);

        groupeDao = Room.databaseBuilder(this, UserDataBase.class, "mi-database.db").allowMainThreadQueries()
                .build().getGroupeDao();

        btn_creerGroupe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();

            }
        });

        mBottomNavigationView=findViewById(R.id.bottom_navigation);
        mBottomNavigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                Log.d("MesLogs", "Bottom ok");
                switch (item.getItemId()){
                    case R.id.action_profile:
                        Log.d("MesLogs", "Profile ok");
                        Intent intent = new Intent(HistoryActivity.this, ProfilepageActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.action_game:
                        Log.d("MesLogs", "Game ok");
                        Intent game = new Intent(HistoryActivity.this, GameActivity.class);
                        startActivity(game);
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_content, selectedFragment);
                transaction.commit();
                return true;
            }
        });
    }

    public void openDialog(){
        DialogActivity dialog = new DialogActivity();
        dialog.show(getSupportFragmentManager(), "groupe dialog");
    }


    public void applyTexts(String groupeName) {
        Groupe groupe = new Groupe(groupeName);
        groupeDao.insert(groupe);
        textViewUsername.setText(groupe.getGroupeName());

    }

/*    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        v = inflater.inflate(R.layout.frament_main_item, container, false);

        TextView nomG = v.findViewById(R.id.nom_quizz);
        TextView nbG = v.findViewById(R.id.nb_quizz);
        TextView membreG = v.findViewById(R.id.membre_quizz);

        int random = new Random().nextInt(80) + 20;
        nbG.setText("Nb membres: "+random);
        if(random < 40){
            membreG.setText("Membre ?: True");
        }else{
            membreG.setText("Membre ?: False");
        }

        RecyclerView recyclerview = v.findViewById(R.id.fragment_main_recycler_view);
        HistoryAdapter adapter = new HistoryAdapter(this.getContext());
        recyclerview.setAdapter(adapter);
        recyclerview.setLayoutManager(new LinearLayoutManager(this.getContext()));

        historyViewHolder = new ViewModelProvider(this).get(HistoryViewholder.class);

        Button button = v.findViewById(R.id.creerGroupe);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
                EditText editText = v.findViewById(R.id.edit_groupname);
                String name = editText.getText().toString();

                HistoryViewholder.insert(new Groupe(name);

                editText.setText("");
            }
        });

        return v;
    }*/

}
