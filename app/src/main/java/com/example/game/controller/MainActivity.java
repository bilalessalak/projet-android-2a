package com.example.game.controller;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.room.Room;

        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.Button;
        import android.widget.Toast;


        import com.example.game.R;
        import com.example.game.data.UserDao;
        import com.example.game.data.UserDataBase;
        import com.example.game.model.User;

public class MainActivity extends AppCompatActivity {
    private EditText edit_nom;
    private EditText edit_mdp;
    private Button btn_sign_up;
    private Button btn_login;
    UserDao db;
    UserDataBase dataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edit_nom = findViewById(R.id.edit_nom);
        edit_mdp = findViewById(R.id.edit_mdp);
        btn_login = findViewById(R.id.button_login);
        btn_sign_up = findViewById(R.id.button_signup);

        dataBase = Room.databaseBuilder(this, UserDataBase.class, "mi-database.db")
                .allowMainThreadQueries().build();

        db = dataBase.getUserDao();

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = edit_nom.getText().toString().trim();
                String password = edit_mdp.getText().toString().trim();

                User user = db.getUser(nom, password);
                if (user != null) {
                    Intent i = new Intent(MainActivity.this, ProfilepageActivity.class);
                    i.putExtra("User", user);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(MainActivity.this, "Unregistered user, or incorrect", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}

