package com.example.game.controller;

    import androidx.annotation.NonNull;
    import androidx.appcompat.app.AppCompatActivity;
    import androidx.fragment.app.Fragment;
    import androidx.fragment.app.FragmentTransaction;
    import androidx.room.Room;

    import android.content.Intent;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.TextView;
    import android.widget.Toast;

    import com.example.game.data.UserDao;
    import com.example.game.data.UserDataBase;
    import com.example.game.model.User;
    import com.example.game.R;
    import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ProfilepageActivity extends AppCompatActivity {
    private TextView nameUser, emailUser;
    private EditText edit_name, edit_email;
    private Button button_save, button_deconnexion;
    private BottomNavigationView mBottomNavigationView;

    private User user;
    UserDao db;
    UserDataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilpage);


        user = (User) getIntent().getSerializableExtra("User");

        nameUser = findViewById(R.id.nameUser);
        emailUser = findViewById(R.id.emailUser);

        edit_name = findViewById(R.id.edit_name);
        edit_email = findViewById(R.id.edit_email);

        button_save = findViewById(R.id.button_save);
        button_deconnexion = findViewById(R.id.button_deconnexion);

        dataBase = Room.databaseBuilder(this, UserDataBase.class, "mi-database.db")
                .allowMainThreadQueries().build();

        db = dataBase.getUserDao();

        if(user != null){
            nameUser.setText("Pseudo: "+user.getUserName());
            emailUser.setText("Email: "+user.getEmail());
        }



        button_deconnexion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {


                //String nom = edit_name.getText().toString().trim();
                //String email = edit_email.getText().toString().trim();

                //user = db.getUser(nom, email);  #fonctionnalité delete possible à implementer
                //db.delete(user);

                Toast.makeText(ProfilepageActivity.this, "Compte deconnecté", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(ProfilepageActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = edit_name.getText().toString().trim();
                String email = edit_email.getText().toString().trim();

                String uNom = user.getUserName();
                String uMail = user.getEmail();

                User user = db.getUser(uNom, uMail);
                if (user != null && !nom.isEmpty() && !email.isEmpty()) {
                    user.setUserName(nom);
                    user.setEmail(email);
                    db.update(user);
                    nameUser.setText("Pseudo: "+user.getUserName());
                    emailUser.setText("Email: "+user.getEmail());
                }else{
                    Toast.makeText(ProfilepageActivity.this, "Champs vides ou user inexistant", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mBottomNavigationView=findViewById(R.id.bottom_navigation);
        mBottomNavigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                Log.d("MesLogs", "Navigation ok");
                switch (item.getItemId()){

                    case R.id.action_profile:
                        Log.d("MesLogs", "Groupe ok");
                        Intent profile = new Intent(ProfilepageActivity.this, ProfilepageActivity.class);
                        startActivity(profile);
                        break;

                    case R.id.action_groupe:
                        Log.d("MesLogs", "Groupe ok");
                        Intent history = new Intent(ProfilepageActivity.this, HistoryActivity.class);
                        startActivity(history);
                        break;

                    case R.id.action_game:
                        Log.d("MesLogs", "Game ok");
                        Intent game = new Intent(ProfilepageActivity.this, GameActivity.class);
                        startActivity(game);
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_content, selectedFragment);
                transaction.commit();
                return true;
            }
        });
    }
}