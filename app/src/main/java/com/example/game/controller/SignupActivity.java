package com.example.game.controller;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.room.Room;

        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.Toast;

        import com.example.game.data.UserDao;
        import com.example.game.data.UserDataBase;
        import com.example.game.model.User;

        import com.example.game.R;

public class SignupActivity extends AppCompatActivity {
    private ImageView btn_retour;
    private EditText edit_pseudo, edit_email, edit_mdp, edit_mdp2;
    private Button button_valider;

    UserDao userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        btn_retour = findViewById(R.id.btn_retour);
        edit_pseudo = findViewById(R.id.edit_pseudo);
        edit_email = findViewById(R.id.edit_email);
        edit_mdp = findViewById(R.id.edit_mdp);
        edit_mdp2 = findViewById(R.id.edit_mdp2);
        button_valider = findViewById(R.id.button_valider);

        userDao = Room.databaseBuilder(this, UserDataBase.class, "mi-database.db").allowMainThreadQueries()
                .build().getUserDao();

        button_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = edit_pseudo.getText().toString().trim();
                String email = edit_email.getText().toString().trim();
                String password = edit_mdp.getText().toString().trim();
                String passwordConf = edit_mdp2.getText().toString().trim();

                if (password.equals(passwordConf)) {
                    User user = new User(userName,password,email);
                    userDao.insert(user);
                    Intent moveToHome = new Intent(SignupActivity.this, ProfilepageActivity.class);
                    moveToHome.putExtra("User", user);
                    startActivity(moveToHome);

                } else {
                    Toast.makeText(SignupActivity.this, "Password is not matching", Toast.LENGTH_SHORT).show();

                }
            }
        });

        btn_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent moveToLogin = new Intent(SignupActivity.this, MainActivity.class);
                startActivity(moveToLogin);
            }
        });
    }
}