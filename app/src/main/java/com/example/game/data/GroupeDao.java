
package com.example.game.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.game.model.Groupe;
import com.example.game.model.GroupeWithUser;
import com.example.game.model.UserWithGroupe;

import java.util.List;


@Dao
public interface GroupeDao {
    @Query("SELECT * FROM Groupe where groupeName= :name")
    Groupe getGroupe(String name);

    @Insert
    void insert(Groupe user);

    @Update
    void update(Groupe user);

    @Delete
    void delete(Groupe user);

    @Query("SELECT * FROM Groupe")
    List<Groupe> getAll();

    @Transaction
    @Query("SELECT * FROM Groupe")
    public List<GroupeWithUser> getGroupeWithUser();

    @Transaction
    @Query("SELECT * FROM User")
    public List<UserWithGroupe> getUserWithGroup();
}

