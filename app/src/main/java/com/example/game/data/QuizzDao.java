package com.example.game.data;

    import androidx.room.Dao;
    import androidx.room.Delete;
    import androidx.room.Insert;
    import androidx.room.Query;
    import androidx.room.Transaction;
    import androidx.room.Update;

    import com.example.game.model.Quizz;
    import com.example.game.model.QuizzWithQuestions;

    import java.util.List;


@Dao
    public interface QuizzDao {
        @Transaction
        @Query("SELECT * FROM Quizz")
        public List<QuizzWithQuestions> getQuizzWithQuestions();

        @Insert
        void insert(Quizz quizz);

        @Update
        void update(Quizz quizz);

        @Delete
        void delete(Quizz quizz);
}
