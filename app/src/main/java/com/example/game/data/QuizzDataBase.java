package com.example.game.data;

    import androidx.room.Database;
    import androidx.room.RoomDatabase;

    import com.example.game.model.Question;
    import com.example.game.model.Quizz;

    @Database(entities = {Quizz.class, Question.class}, version = 1, exportSchema = false)
    public abstract class QuizzDataBase extends RoomDatabase {

        public abstract QuizzDao getQuizzDao();

}