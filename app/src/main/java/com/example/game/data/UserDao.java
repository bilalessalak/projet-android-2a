package com.example.game.data;

    import androidx.room.Dao;
    import androidx.room.Delete;
    import androidx.room.Insert;
    import androidx.room.Query;
    import androidx.room.Update;

    import com.example.game.model.User;


    @Dao
    public interface UserDao {
        @Query("SELECT * FROM User where userName= :name and password= :password")
        User getUser(String name, String password);

        @Insert
        void insert(User user);

        @Update
        void update(User user);

        @Delete
        void delete(User user);
}
