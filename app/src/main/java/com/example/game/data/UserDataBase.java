package com.example.game.data;

    import androidx.room.Database;
    import androidx.room.RoomDatabase;

    import com.example.game.model.Groupe;
    import com.example.game.model.User;
    import com.example.game.model.UserGroupeCrossRef;


@Database(entities = {User.class, Groupe.class, UserGroupeCrossRef.class}, version = 1, exportSchema = false)
    public abstract class UserDataBase extends RoomDatabase {

        public abstract UserDao getUserDao();

        public abstract GroupeDao getGroupeDao();

}