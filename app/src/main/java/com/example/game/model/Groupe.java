package com.example.game.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Groupe {

    @PrimaryKey(autoGenerate = true)
    private long groupeId;
    private String groupeName;

    public Groupe(String groupeName){
        this.groupeName = groupeName;
    }

    public long getGroupeId() {
        return groupeId;
    }

    public void setGroupeId(long id) {
        this.groupeId = id;
    }

    public String getGroupeName() {
        return groupeName;
    }

    public void setGroupeName(String groupeName) {
        this.groupeName = groupeName;
    }
}
