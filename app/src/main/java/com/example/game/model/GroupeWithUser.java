
package com.example.game.model;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class GroupeWithUser {

    @Embedded public Groupe groupe;
    @Relation(
            parentColumn = "groupeId",
            entityColumn = "UserId",
            associateBy = @Junction(UserGroupeCrossRef.class)
    )
    public List<User> users;
}
