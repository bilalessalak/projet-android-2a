package com.example.game.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class Question {
    @PrimaryKey
    public long questionId;

    public long quizzHomeId;
    public String enonce;
    public String rep_1;
    public String rep_2;
    public String rep_3;
    public String rep_4;
    public String bonne_reponse;
}

/*public class Question { //cette classe s'occupe de l'instanciation d'une question
    private final String mQuestion;
    private final List<String> mChoiceList;
    private final int mAnswerIndex;

    public Question(String mQuestion, List<String> mChoiceList, int mAnswerIndex) {
        this.mQuestion = mQuestion;
        this.mChoiceList = mChoiceList;
        this.mAnswerIndex = mAnswerIndex;
    }

    public String getQuestion() { //return l'énoncé
        return mQuestion;
    }

    public List<String> getmChoiceList() { //return les choix
        return mChoiceList;
    }

    public int getmAnswerIndex() { //return l'emplacement de la réponse dans la liste des choix
        return mAnswerIndex;
    }
}*/
