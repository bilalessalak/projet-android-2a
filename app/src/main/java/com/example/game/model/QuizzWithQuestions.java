package com.example.game.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class QuizzWithQuestions {
    @Embedded public Quizz quizz;
    @Relation(
            parentColumn = "quizzId",
            entityColumn = "quizzHomeId"
    )
    public List<Question> questions;

    public List<Question> getQuestions() {
        return this.questions;
    }
}
