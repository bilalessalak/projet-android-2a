package com.example.game.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(primaryKeys = {"UserId", "groupeId"})
public class UserGroupeCrossRef {

        public long UserId;
        @ColumnInfo(index = true)
        public long groupeId;

        public UserGroupeCrossRef(long UserId, long groupeId){
                this.UserId = UserId;
                this.groupeId = groupeId;
        }

}
