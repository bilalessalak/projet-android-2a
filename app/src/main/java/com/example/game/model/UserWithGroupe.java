
package com.example.game.model;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class UserWithGroupe {

    @Embedded public User User;
    @Relation(
            parentColumn = "UserId",
            entityColumn = "groupeId",
            associateBy = @Junction(UserGroupeCrossRef.class)
    )
    public List<Groupe> mesgroupes;
}

