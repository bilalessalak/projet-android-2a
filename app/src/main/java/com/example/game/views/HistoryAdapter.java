package com.example.game.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.game.R;
import com.example.game.model.User;

import java.util.List;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewholder> {

    private List<User> users;
    private static Context context;


    public HistoryAdapter(List<User> users) {
        this.users = users;
    }

    @Override
    public HistoryViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.frament_main_item, parent, false);


        return new HistoryViewholder(view);
    }

    @Override
    public void onBindViewHolder(HistoryViewholder viewHolder, int position) {
        viewHolder.updateWithUser(this.users.get(position));
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

}