package com.example.game.views;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.game.R;
import com.example.game.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryViewholder extends RecyclerView.ViewHolder {

    @BindView(R.id.nom_quizz)
    TextView textView;

    public HistoryViewholder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void updateWithUser(User user){
        this.textView.setText(user.getUserName());
    }
}